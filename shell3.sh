#!/bin/sh
L="/var/lib/jenkins/workspace/MultiPipeline_App"
x=$(find $L -name "*.jar")
jar -cvf combined.ear $x

M="/var/lib/jenkins/workspace/MultiPipeline_Repo1"
y=$(find $M -name "*.jar")
jar -uvf combined.ear $y

N="/var/lib/jenkins/workspace/MultiPipeline_Repo2"
z=$(find $N -name "*.jar")
jar -uvf combined.ear $z
